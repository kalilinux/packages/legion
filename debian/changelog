legion (0.4.3-0kali7) kali-dev; urgency=medium

  * Update upstream URL
  * cutycapt is not available on armel

 -- Arnaud Rebillout <arnaudr@kali.org>  Wed, 26 Feb 2025 10:29:37 +0700

legion (0.4.3-0kali6) kali-dev; urgency=medium

  * Fix various Python 3.12 SyntaxWarning

 -- Arnaud Rebillout <arnaudr@kali.org>  Mon, 16 Sep 2024 16:02:41 +0700

legion (0.4.3-0kali5) kali-dev; urgency=medium

  * Re-add the patch use-cutycapt-for-arm.patch

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 27 Aug 2024 11:27:45 +0200

legion (0.4.3-0kali4) kali-dev; urgency=medium

  * Refresh patches
  * Do not use the obsolete package rwho (see Debian bug 1074175)

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 26 Aug 2024 17:02:28 +0200

legion (0.4.3-0kali3) kali-dev; urgency=medium

  [ Arnaud Rebillout ]
  * Depends: also allow rsh-client

  [ Daniel Ruiz de Alegría ]
  * Revert "Replace imagemagick with graphicsmagick-imagemagick-compat"

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Mon, 05 Feb 2024 12:45:12 +0100

legion (0.4.3-0kali2) kali-dev; urgency=medium

  * Depends: Replace rsh-client with rsh-redone-client

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 02 Feb 2024 15:01:54 +0700

legion (0.4.3-0kali1) kali-dev; urgency=medium

  * New upstream version 0.4.3
  * Refresh patches
  * Add dependency python3-urllib3
  * Fix arm build

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Tue, 21 Nov 2023 11:47:51 +0100

legion (0.4.1-0kali4) kali-dev; urgency=medium

  * Replace cutycapt with eyewitness
  * Replace imagemagick with graphicsmagick-imagemagick-compat
  * Recover deleted patch Use-session-user-to-open-firefox.patch
  * Remove sra-toolkit as a dependency
  * Refresh patches

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Fri, 17 Nov 2023 14:19:44 +0100

legion (0.4.1-0kali3) kali-dev; urgency=medium

  * package is arch all: move sra-toolkit to Recommends as it's only
    available on arm64 and amd64

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 16 Nov 2023 15:38:15 +0100

legion (0.4.1-0kali2) kali-dev; urgency=medium

  * d/control: sra-toolkit is now only available on amd64 and arm64.

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 16 Nov 2023 13:53:15 +0100

legion (0.4.1-0kali1) kali-dev; urgency=medium

  * Use monospace font
  * Update debian/watch
  * New upstream version 0.4.1

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Wed, 15 Nov 2023 17:10:38 +0100

legion (0.4.0-0kali2) kali-dev; urgency=medium

  * Remove phantomjs dependency as it's archived
  * Add missing import in patch

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Wed, 15 Nov 2023 14:19:07 +0100

legion (0.4.0-0kali1) kali-experimental; urgency=medium

  * New upstream version 0.4.0
  * Update debian/control
  * Update dependencies
  * Update install scripts
  * Update Do-no-use-pip-to-upgrade.patch
  * Add lintian-overrides
  * Patch to use python3 shebangs
  * Add patch to use xdg tools
  * Fix paths to multiple files and binaries
  * Add extra external tools as dependencies
  * Update remaining patches
  * Fix dirbuster path
  * Add all dependencies for all tools required in legion.conf
  * Fix typo
  * Fix legion tries to write to /usr/share/legion
  * More fixes for legion trying to write to /usr folder

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Mon, 13 Nov 2023 14:27:24 +0100

legion (0.3.9-0kali1) kali-dev; urgency=medium

  * New upstream version 0.3.9
  * Refresh patches
  * Add a patch to not use pip in Kali
  * Add minimal version of python3-pyexploitdb
  * Remove sparta-scripts from Suggests (already in Depends)
  * Bump Standards-Version to 4.6.1

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 13 Oct 2022 15:10:22 +0200

legion (0.3.8-0kali5) kali-dev; urgency=medium

  * Improve patch for PyQt5

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 18 May 2022 14:39:35 +0200

legion (0.3.8-0kali4) kali-dev; urgency=medium

  * Fix typo in debian/control

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 28 Apr 2022 11:29:31 +0200

legion (0.3.8-0kali3) kali-dev; urgency=medium

  [ Ben Wilson ]
  * Fix typo
  * Add sparta-scripts as suggestion
  * Add executable to helper-script
  * New helper-script format
  * Remove template comment and switch spaces to tabs

  [ Sophie Brun ]
  * Refresh patches
  * Fix arguments type issue
  * Force update of python3-quamash

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 28 Apr 2022 11:24:02 +0200

legion (0.3.8-0kali2) kali-dev; urgency=medium

  * Remove problematic QT env variables from helper script
  * Use xdg-open to open files using currently installed programs
  * Fix open terminal
  * Remove mousepad, ristretto and eog from dependencies
  * Fix firefox patch
  * Update list of patches in debian/patches/series

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Thu, 15 Jul 2021 11:26:54 +0200

legion (0.3.8-0kali1) kali-dev; urgency=medium

  * Add a patch to open firefox
  * New upstream version 0.3.8
  * Bump Standards-Version to 4.5.1

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 03 Jun 2021 10:10:28 +0200

legion (0.3.7-0kali2) kali-dev; urgency=medium

  * Fix issue with conf file in /usr or /etc
  * Force sudo usage to run legion
  * Reorder patches
  * Add a patch to use absolute path for nmap.xsl file

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 16 Sep 2020 09:58:28 +0200

legion (0.3.7-0kali1) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Sophie Brun ]
  * New upstream version 0.3.7
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 11 Sep 2020 13:44:45 +0200

legion (0.3.6b-0kali2) kali-dev; urgency=medium

  * Add figlet in depends: legion fails if the package is not installed (it is
    only in "Recommends" of python3-pyfiglet package"). See 6221.

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 26 Mar 2020 14:29:12 +0100

legion (0.3.6b-0kali1) kali-dev; urgency=medium

  * New upstream version 0.3.6b
  * Refresh patches for new Release
  * Update minimal required version of python3-pyexploitdb
  * debian/rules: Create missing empty log files
  * Bump Standards-Version to 4.5.0

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 23 Mar 2020 17:50:48 +0100

legion (0.3.6-0kali2) kali-dev; urgency=medium

  * Configure git-buildpackage for Kali
  * Add GitLab's CI configuration file
  * Update used tools: ristretto and mousepad instead of eog and leafpad
  * Bump Standards-Version to 4.4.1

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 13 Jan 2020 15:00:02 +0100

legion (0.3.6-0kali1) kali-dev; urgency=medium

  * Initial release (issue 5642)

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 16 Sep 2019 14:35:11 +0200
